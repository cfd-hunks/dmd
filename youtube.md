# Descripción de video

Este video es parte de una serie que puedes encontrar en 

https://www.youtube.com/playlist?list=PLf1MPu5-32TIKHmPdLhKEXtTwAAbg5vRR

La descripción de esta serie la encuentras en 

https://gitlab.com/cfd-hunks/dmd/-/blob/master/readme.md

En casi todos los casos hay material complementario que está el
directorio correspondiente (al número de video), en

https://gitlab.com/cfd-hunks/dmd

Se aceptan los comentarios por cualquier vía (email, comentarios de
youtube, etc.), pero se recomienda el uso de "issues de gitlab"

https://gitlab.com/cfd-hunks/dmd/-/issues

Con esto se busca que estén juntos los comentarios al
respecto de un mismo punto, y que sean públicos.

# Descripción de la lista

Estos son algunos videos que se usaron en el curso de "Dinámica de Medios Deformables", impartido a finales de 2020, por Roberto Velasco Segura y Federico Hernández Sánchez, en la Facultad de Ciencias, de la Universisdad Nacional Autónoma de México.

La descripción completa de esta serie la encuentras en 

https://gitlab.com/cfd-hunks/dmd/-/blob/master/readme.md

En casi todos los casos hay material complementario que está el directorio correspondiente (al número de video), en

https://gitlab.com/cfd-hunks/dmd

Se aceptan los comentarios por cualquier vía (email, comentarios de youtube, etc.), pero se recomienda el uso de "issues de gitlab"

https://gitlab.com/cfd-hunks/dmd/-/issues

Con esto se busca que estén juntos los comentarios al respecto de un mismo punto, y que sean públicos.
