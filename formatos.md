Los archivos que están en los directorios de los videos, se pueden
abrir con los siguiente programas:
- .ora : MyPaint (versión ?) http://mypaint.org/
- .xoj : xournal (version ?) http://xournal.sourceforge.net/
- .xopp : xournalpp (version ?) https://github.com/xournalpp/xournalpp
- .md : Este es una archivo de texto plano que está en sintaxis
  Markdown, se puede abrir con cualquier editor de texto plano, se
  recomienda https://atom.io/
  
Si eres usuario de GNU/Linux se recomienda que instales los paquetes
de tu repositorio, usando `apt`, `yum`, `pacman`, etc.





